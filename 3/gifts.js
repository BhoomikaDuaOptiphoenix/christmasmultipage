(function(win) {
    'use strict';

    var listeners = [],
        doc = win.document,
        MutationObserver = win.MutationObserver || win.WebKitMutationObserver,
        observer;

    function ready(selector, fn) {
        // Store the selector and callback to be monitored
        listeners.push({
            selector: selector,
            fn: fn
        });
        if (!observer) {
            // Watch for changes in the document
            observer = new MutationObserver(check);
            observer.observe(doc.documentElement, {
                childList: true,
                subtree: true
            });
        }
        // Check if the element is currently in the DOM
        check();
    }

    function check() {
        // Check the DOM for elements matching a stored selector
        for (var i = 0, len = listeners.length, listener, elements; i < len; i++) {
            listener = listeners[i];
            // Query for elements matching the specified selector
            elements = doc.querySelectorAll(listener.selector);
            for (var j = 0, jLen = elements.length, element; j < jLen; j++) {
                element = elements[j];
                // Make sure the callback isn't invoked with the
                // same element more than once
                if (!element.ready) {
                    element.ready = true;
                    // Invoke the callback with the element
                    listener.fn.call(element, element);
                }
            }
        }
    }

    // Expose 'ready'
    win.optiReady = ready;

})(this);



(function() {
    var config = {
        selectors: {
            productListing: '[data-test="productListingPage-results"]'
        },
        html: {
            christmasBanner: ['',
                '    <article role="article" class="css-1lmbuna es2a5et0">',
                '    <a href="https://www.harrods.com/en-gb/gifts" target="_blank" rel="nofollow  noopener" aria-label="Read Article">',
                '        <picture>',
                '            <source srcset="https://harrods.com/BWStaticContent/50000/9b924471-1c9d-48c7-881d-8e7ead6e4e7a_m-megamenu-gift-boxes.jpg" media="(max-width: 47.94em)">',
                '            <source srcset="https://harrods.com/BWStaticContent/50000/9b924471-1c9d-48c7-881d-8e7ead6e4e7a_m-megamenu-gift-boxes.jpg" media="(min-width: 48em) and (max-width: 63.94em)">',
                '            <source srcset="https://harrods.com/BWStaticContent/50000/850d9a40-e46c-489f-afa9-f4318407fe3f_d-sd-megamenu-gift-boxes.jpg" media="(min-width: 64em) and (max-width: 89.94em)">',
                '            <source srcset="https://harrods.com/BWStaticContent/50000/850d9a40-e46c-489f-afa9-f4318407fe3f_d-sd-megamenu-gift-boxes.jpg" media="(min-width: 90em)"><img src="https://harrods.com/BWStaticContent/50000/850d9a40-e46c-489f-afa9-f4318407fe3f_d-sd-megamenu-gift-boxes.jpg" srcset="" data-src="https://harrods.com/BWStaticContent/50000/850d9a40-e46c-489f-afa9-f4318407fe3f_d-sd-megamenu-gift-boxes.jpg"',
                '                alt="" loading="lazy" class="es2a5et7 e130wn470 e1ji98wl1 css-14c1ny5 e1nmxst70"><noscript></noscript></picture>',
                '        <div class="css-1cm4bpx es2a5et1">',
                '            <p class="css-11s5d32 es2a5et2"> </p>',
                '            <p class="css-1x9nwhd es2a5et3">Complimentary Gift Box</p>',
                '            <p class="css-tqm1da es2a5et4">Buying a gift? You can choose to have your order sent in a gift box in checkout.</p><span class="css-9d5t6p es2a5et6">Shop Christmas</span></div>',
                '      </a>',
                '</article>',
                ''
            ].join('')
        }
    };

    function allHTMLchanges() {
        optiReady(config.selectors.productListing, function(element) {

            var div = document.createElement('div');
            div.innerHTML = config.html.christmasBanner.trim();
            let length = element.childNodes.length;
            if (15 < length) {
                element.childNodes[15].after(div);
            } else {
                element.childNodes[length].after(div);
            }
        });
    }


    function init() {
        allHTMLchanges();
    }
    init();
})();