(function(win) {
    'use strict';

    var listeners = [],
        doc = win.document,
        MutationObserver = win.MutationObserver || win.WebKitMutationObserver,
        observer;

    function ready(selector, fn) {
        // Store the selector and callback to be monitored
        listeners.push({
            selector: selector,
            fn: fn
        });
        if (!observer) {
            // Watch for changes in the document
            observer = new MutationObserver(check);
            observer.observe(doc.documentElement, {
                childList: true,
                subtree: true
            });
        }
        // Check if the element is currently in the DOM
        check();
    }

    function check() {
        // Check the DOM for elements matching a stored selector
        for (var i = 0, len = listeners.length, listener, elements; i < len; i++) {
            listener = listeners[i];
            // Query for elements matching the specified selector
            elements = doc.querySelectorAll(listener.selector);
            for (var j = 0, jLen = elements.length, element; j < jLen; j++) {
                element = elements[j];
                // Make sure the callback isn't invoked with the
                // same element more than once
                if (!element.ready) {
                    element.ready = true;
                    // Invoke the callback with the element
                    listener.fn.call(element, element);
                }
            }
        }
    }

    // Expose 'ready'
    win.optiReady = ready;

})(this);



optiReady('[data-test="continue-shopping"]', function(element) {
    var html = `<div class="custom-christmas-checkout-message">
                <img src="https://harrods.com/BWStaticContent/50000/f9141f60-2dd3-4945-b23b-575e0aa090c4_gift-box-icon.jpg" alt="Girl in a jacket">
                <p>Buying a gift? You can choose to have your order sent in a gift box in checkout.</p>
                </div>`;
    var div = document.createElement('div');
    div.innerHTML = html.trim();
    element.insertAdjacentElement('afterend', div);
    document.querySelector('.custom-christmas-checkout-message').parentNode.classList.add('custom-christmas-checkout-message-parent')
    document.querySelectorAll('.custom-christmas-checkout-message')[1].remove()
});