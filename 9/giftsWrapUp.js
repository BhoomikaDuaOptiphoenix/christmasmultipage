(function(win) {
    'use strict';

    var listeners = [],
        doc = win.document,
        MutationObserver = win.MutationObserver || win.WebKitMutationObserver,
        observer;

    function ready(selector, fn) {
        // Store the selector and callback to be monitored
        listeners.push({
            selector: selector,
            fn: fn
        });
        if (!observer) {
            // Watch for changes in the document
            observer = new MutationObserver(check);
            observer.observe(doc.documentElement, {
                childList: true,
                subtree: true
            });
        }
        // Check if the element is currently in the DOM
        check();
    }

    function check() {
        // Check the DOM for elements matching a stored selector
        for (var i = 0, len = listeners.length, listener, elements; i < len; i++) {
            listener = listeners[i];
            // Query for elements matching the specified selector
            elements = doc.querySelectorAll(listener.selector);
            for (var j = 0, jLen = elements.length, element; j < jLen; j++) {
                element = elements[j];
                // Make sure the callback isn't invoked with the
                // same element more than once
                if (!element.ready) {
                    element.ready = true;
                    // Invoke the callback with the element
                    listener.fn.call(element, element);
                }
            }
        }
    }

    // Expose 'ready'
    win.optiReady = ready;

})(this);






(function() {
    var config = {
        selectors: {
            main: 'main'
        },
        html: {
            christmasBanner: ['',
                '    <div class="custom-christmas-wrapup-block">',
                '    <h2>Your Christmas Wrapped Up</h2>',
                '    <div class="custom-christmas-nuggets">',
                '        <div class="custom-nugget">',
                '            <h2>Gift Wrapping</h2>',
                '            <p>Buying a gift? You can choose to have your gift boxed in checkout.</p>',
                '        </div>',
                '        <div class="custom-nugget"></div>',
                '        <div class="custom-nugget">',
                '            <a class="custom-no-underline" href="https://preview-harrods.blackandwhite-ff.com/en-ca/shopping/gift-cards">',
                '                <h2>Gift Experiences</h2>',
                '            </a>',
                '            <p>For a gift that keeps on giving, look no further than our array of memory-making Harrods Experience gift cards.</p>',
                '            <a href="https://preview-harrods.blackandwhite-ff.com/en-ca/shopping/gift-cards">Discover</a>',
                '        </div>',
                '        <div class="custom-nugget"></div>',
                '        <div class="custom-nugget">',
                '            <a class="custom-no-underline" href="https://www.harrods-experiences.com/experiences">',
                '                <h2>Gift Cards</h2>',
                '            </a>',
                '            <p>Let loved ones do the choosing with a Harrods gift card, redeemable in-store and online.</p>',
                '            <a href="https://www.harrods-experiences.com/experiences">Shop Gift Cards</a>',
                '        </div>',
                '    </div>',
                '  </div>',
                ''
            ].join('')
        }
    };

    function allHTMLchanges() {

        optiReady(config.selectors.main, function(element) {
            var div = document.createElement('div');
            div.innerHTML = config.html.christmasBanner.trim();
            element.appendChild(div);
        });
    }


    function init() {
        allHTMLchanges();
    }
    init();
})();