(function(win) {
    'use strict';

    var listeners = [],
        doc = win.document,
        MutationObserver = win.MutationObserver || win.WebKitMutationObserver,
        observer;

    function ready(selector, fn) {
        // Store the selector and callback to be monitored
        listeners.push({
            selector: selector,
            fn: fn
        });
        if (!observer) {
            // Watch for changes in the document
            observer = new MutationObserver(check);
            observer.observe(doc.documentElement, {
                childList: true,
                subtree: true
            });
        }
        // Check if the element is currently in the DOM
        check();
    }

    function check() {
        // Check the DOM for elements matching a stored selector
        for (var i = 0, len = listeners.length, listener, elements; i < len; i++) {
            listener = listeners[i];
            // Query for elements matching the specified selector
            elements = doc.querySelectorAll(listener.selector);
            for (var j = 0, jLen = elements.length, element; j < jLen; j++) {
                element = elements[j];
                // Make sure the callback isn't invoked with the
                // same element more than once
                if (!element.ready) {
                    element.ready = true;
                    // Invoke the callback with the element
                    listener.fn.call(element, element);
                }
            }
        }
    }

    // Expose 'ready'
    win.optiReady = ready;

})(this);





(function() {
    var config = {
        selectors: {
            originalArticle: 'article[data-test="articleModule-article"]'
        },
        html: {
            christmasStoryBanner: ['',
                '<p data-test="articleModule-article-tag" class="css-mpz3jj e77kyxb2">THE PERFECT TOUCH</p>',
                '<h2 data-test="articleModule-article-title" class="css-kgklj3 e77kyxb3">Complimentary Gift Box</h2>',
                '<div class="css-15m8l8n e77kyxb4" style="transform: scaleY(1);">',
                '<div class="css-1ong7vw e77kyxb5" style="transform: scaleY(1);"><img alt="" srcset="https://harrods.com/BWStaticContent/50000/', 'f0bfcd94-c296-4abd-86a3-15a703239959_d-pdp-promo-gift-boxes.jpg 70w, https://harrods.com/BWStaticContent/50000/', 'f0bfcd94-c296-4abd-86a3-15a703239959_d-pdp-promo-gift-boxes.jpg 170w, https://harrods.com/BWStaticContent/50000/', 'f0bfcd94-c296-4abd-86a3-15a703239959_d-pdp-promo-gift-boxes.jpg 500w, https://harrods.com/BWStaticContent/50000/', 'f0bfcd94-c296-4abd-86a3-15a703239959_d-pdp-promo-gift-boxes.jpg 1000w"',
                ' src="https://harrods.com/BWStaticContent/50000/f0bfcd94-c296-4abd-86a3-15a703239959_d-pdp-promo-gift-boxes.jpg" sizes="100vw" ', 'loading="lazy" data-test="articleModule-article-image" class="e77kyxb6 e130wn470 css-xpsz87 e1ji98wl0"><noscript></noscript></div > ',
                '</div><span class="css-9ycejs e77kyxb7"><p data-test="articleModule-article-authorLabel" class="css-1o9ng4g e77kyxb8">Buying a gift? You ', 'can choose to have your order sent in a gift box in checkout.',
                '</p><address data-test="articleModule-article-authorAddress" class="css-he37sl e77kyxb9"></address></span>',
                '<a class="css-1mdlqs3 e77kyxb11" data-test="articleModule-article-cta" href="https://www.harrods.com/en-gb/gifts">Shop Christmas</a>',
                ''
            ].join('')
        }
    };

    function allHTMLchanges() {
        optiReady(config.selectors.originalArticle, function(element) {
            element.innerHTML = config.html.christmasStoryBanner.trim();
        });
    }


    function init() {
        allHTMLchanges();
    }
    init();
})();