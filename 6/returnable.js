(function(win) {
    'use strict';

    var listeners = [],
        doc = win.document,
        MutationObserver = win.MutationObserver || win.WebKitMutationObserver,
        observer;

    function ready(selector, fn) {
        // Store the selector and callback to be monitored
        listeners.push({
            selector: selector,
            fn: fn
        });
        if (!observer) {
            // Watch for changes in the document
            observer = new MutationObserver(check);
            observer.observe(doc.documentElement, {
                childList: true,
                subtree: true
            });
        }
        // Check if the element is currently in the DOM
        check();
    }

    function check() {
        // Check the DOM for elements matching a stored selector
        for (var i = 0, len = listeners.length, listener, elements; i < len; i++) {
            listener = listeners[i];
            // Query for elements matching the specified selector
            elements = doc.querySelectorAll(listener.selector);
            for (var j = 0, jLen = elements.length, element; j < jLen; j++) {
                element = elements[j];
                // Make sure the callback isn't invoked with the
                // same element more than once
                if (!element.ready) {
                    element.ready = true;
                    // Invoke the callback with the element
                    listener.fn.call(element, element);
                }
            }
        }
    }

    // Expose 'ready'
    win.optiReady = ready;

})(this);


(function() {
    var config = {
        selectors: {
            returnTextDiv: '.css-wiv27h.e23d3bz0 .css-1san3u9.egmhfll0',
            nonReturnLink: '[data-test="returnsAndShipping-shipping-cta"]'
        },
        html: {
            returnText: ['',
                '    <div class="css-1san3u9 egmhfll0">',
                '    <div class="e23d3bz2 css-vsg9ku egmhfll0">',
                '        <div>',
                '            <h3 data-test="returnsAndShipping-returns-title" class="css-1u2aqvv e93vans0">Free Delivery</h3>',
                '            <p data-test="returnsAndShipping-returns-text" class="css-55dd19 e93vans1">Enjoy free UK delivery on orders of £100 or more and free returns on all orders. Deliveries will take two to four days to arrive. Buying a gift? You can choose to have your order sent in a complimentary gift box at checkout.</p><a class="css-7653i5 e93vans2"',
                '                data-test="returnsAndShipping-returns-cta" href="https://www.harrods.com/en-gb/faqs/delivery">Find Out More</a></div>',
                '    </div>',
                '    <div class="e23d3bz2 css-vsg9ku egmhfll0">',
                '        <div>',
                '            <h3 data-test="returnsAndShipping-shipping-title" class="css-1u2aqvv e93vans0">Returns</h3>',
                '            <p data-test="returnsAndShipping-shipping-text" class="css-55dd19 e93vans1">If you change your mind about your order, you can send it back to us using our free returns service. You have 14 days from the date of delivery to return your order.</p><a class="css-7653i5 e93vans2" data-test="returnsAndShipping-shipping-cta"',
                '                href="/en-gb/faqs/returns">Find Out More</a></div>',
                '    </div>',
                '</div>',
                ''
            ].join(''),
            nonReturnableText: ['',
                '            <div class="css-1san3u9 egmhfll0">',
                '    <div class="e23d3bz2 css-vsg9ku egmhfll0">',
                '        <div>',
                '            <h3 data-test="returnsAndShipping-returns-title" class="css-1u2aqvv e93vans0">Free Delivery</h3>',
                '            <p data-test="returnsAndShipping-returns-text" class="css-55dd19 e93vans1">Enjoy free UK delivery on orders of £100 or more and free returns on all orders. Deliveries will take two to four days to arrive. Buying a gift? You can choose to have your order sent in a complimentary gift box at checkout.</p><a class="css-7653i5 e93vans2"',
                '                data-test="returnsAndShipping-returns-cta" href="https://www.harrods.com/en-gb/faqs/delivery">Find Out More</a></div>',
                '    </div>',
                '    <div class="e23d3bz2 css-vsg9ku egmhfll0">',
                '        <div>',
                '            <h3 data-test="returnsAndShipping-shipping-title" class="css-1u2aqvv e93vans0">Returns</h3>',
                '            <p data-test="returnsAndShipping-shipping-text" class="css-55dd19 e93vans1">Please note that due to the nature of this product it cannot be returned.</p><a class="css-7653i5 e93vans2" data-test="returnsAndShipping-shipping-cta" href="/en-gb/faqs/returns">View Our Returns Policy</a></div>',
                '    </div>',
                '</div>',
                ''
            ].join(''),
            nonReturnableLinkText: 'View Our Returns Policy'
        }
    };

    function allHTMLchanges() {
        optiReady(config.selectors.returnTextDiv, function(element) {

            //Text on orginial links which lets us know if the product is returnable or not
            if (document.querySelector(config.selectors.nonReturnLink).textContent == config.html.nonReturnableLinkText) {
                element.innerHTML = config.html.nonReturnableText;
            } else {
                element.innerHTML = config.html.returnText;
            }
        });
    }


    function init() {
        allHTMLchanges();
    }
    init();
})();