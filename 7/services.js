(function(win) {
    'use strict';

    var listeners = [],
        doc = win.document,
        MutationObserver = win.MutationObserver || win.WebKitMutationObserver,
        observer;

    function ready(selector, fn) {
        // Store the selector and callback to be monitored
        listeners.push({
            selector: selector,
            fn: fn
        });
        if (!observer) {
            // Watch for changes in the document
            observer = new MutationObserver(check);
            observer.observe(doc.documentElement, {
                childList: true,
                subtree: true
            });
        }
        // Check if the element is currently in the DOM
        check();
    }

    function check() {
        // Check the DOM for elements matching a stored selector
        for (var i = 0, len = listeners.length, listener, elements; i < len; i++) {
            listener = listeners[i];
            // Query for elements matching the specified selector
            elements = doc.querySelectorAll(listener.selector);
            for (var j = 0, jLen = elements.length, element; j < jLen; j++) {
                element = elements[j];
                // Make sure the callback isn't invoked with the
                // same element more than once
                if (!element.ready) {
                    element.ready = true;
                    // Invoke the callback with the element
                    listener.fn.call(element, element);
                }
            }
        }
    }

    // Expose 'ready'
    win.optiReady = ready;

})(this);

optiReady('div[data-test="navigation-tags"] .css-1plqyd.exg2pk06', function(element) {
    if (element.innerText == 'Lower Ground Floor — A special gift deserves special wrapping. Our in-store team is prepped and ready to transform your present, large or small, using luxury paper, ribbons and decorations. Items must be collected after 72 hours.') {
        element.innerText = 'Lower Ground Floor — A special gift deserves special wrapping. Heading to the store? Our talented team is prepped and ready to transform your present. Shopping online? Choose to have your order sent in a gorgeous gift box in checkout.';
    }

});

(function() {
    var config = {
        selectors: {
            services: 'div[data-test="navigation-tags"] .css-1plqyd.exg2pk06'
        },
        html: {
            initialText: 'Lower Ground Floor — A special gift deserves special wrapping. Our in-store team is prepped and ready to transform your present, large or small, using luxury paper, ribbons and decorations. Items must be collected after 72 hours.',
            replacementText: 'Lower Ground Floor — A special gift deserves special wrapping. Heading to the store? Our talented team is prepped and ready to transform your present. Shopping online? Choose to have your order sent in a gorgeous gift box in checkout.'
        }
    };

    function allHTMLchanges() {
        optiReady(config.selectors.services, function(element) {
            if (element.innerText == config.html.initialText) {
                element.innerText = config.html.replacementText;
            }
        });
    }


    function init() {
        allHTMLchanges();
    }

    init();
})();